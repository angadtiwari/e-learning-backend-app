var dbName = process.env.DB_NAME;
var dbConnectionUrl = process.env.DB_CONNECTION_URL;

module.exports = {dbName, dbConnectionUrl};