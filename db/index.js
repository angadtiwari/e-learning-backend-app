const {db} = require('./init');
const {dbName} = require('./../constants');

module.exports = (() => {
    var findOne = async (collectionName, condition) => {
        const dbObject = await db();

        const findOnePromise = new Promise((resolve, reject) => {
            dbObject.db(dbName).collection(collectionName).find(condition).limit(1).toArray((err, result) => {
                if (err) {
                    reject(err);
                }
                resolve(result[0]);
                dbObject.close();
            });
        });
        return await findOnePromise.then((values) => {
            return values;
        });
    };

    var findAll = async (collectionName, condition, sortOrder, projectionQuery) => {
        const dbObject = await db();

        const findAllPromise = new Promise((resolve, reject) => {
            dbObject.db(dbName).collection(collectionName).find(condition, {projection: projectionQuery}).sort(sortOrder).toArray((err, result) => {
                if (err) {
                    reject(err);
                }
                resolve(result);
                dbObject.close();
            });
        });
        return await findAllPromise.then((values) => {
            return values;
        });
    };

    var findWithPaging = async (collectionName, condition, skip, page_size, sortOrder={}) => {
        const dbObject = await db();

        const findWithPagingPromise = new Promise((resolve, reject) => {
            dbObject.db(dbName).collection(collectionName).find(condition).sort(sortOrder).skip(skip).limit(page_size).toArray((err, result) => {
                if (err) {
                    reject(err);
                }
                resolve(result);
                dbObject.close();
            });
        });
        return await findWithPagingPromise.then((values) => {
            return values;
        });
    };

    var insertOne = async (collectionName, objectToInsert) => {
        const dbObject = await db();

        const insertOnePromise = new Promise((resolve, reject) => {
            dbObject.db(dbName).collection(collectionName).insertOne(objectToInsert, (err, result) => {
                if (err) {
                    reject(err);
                }
                resolve(result);
                dbObject.close();
            });
        });
        return await insertOnePromise.then((values) => {
            return values;
        });
    };

    var findOneAndUpdate = async (collectionName, condition, objectToInsert, upsert=true) => {
        const dbObject = await db();

        const findOneAndUpdatePromise = new Promise((resolve, reject) => {
            dbObject.db(dbName).collection(collectionName).findOneAndUpdate(condition, {$set: objectToInsert}, {returnOriginal: false, upsert: upsert}, (err, result) => {
                if (err) {
                    reject(err);
                }
                resolve(result.value);
                dbObject.close();
            });
        });
        return await findOneAndUpdatePromise.then((values) => {
            return values;
        }).catch(err => {
            return err;
        });
    };

    var deleteOne = async (collectionName, condition) => {
        const dbObject = await db();
        await dbObject.db(dbName).collection(collectionName).deleteOne(condition);
    };

    return {findOne, findAll, findWithPaging, insertOne, findOneAndUpdate, deleteOne};
})();