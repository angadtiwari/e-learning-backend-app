var mongo = require('mongodb');
var MongoClient = mongo.MongoClient;
const {dbConnectionUrl} = require('./../constants');

module.exports = (() => {
    var db = async () => {
        var dbPromise = new Promise((resolve, reject) => {
            MongoClient.connect(dbConnectionUrl, { useUnifiedTopology: true, useNewUrlParser: true }, (err, db) => {
                if (err) {
                    reject(err);
                }
                resolve(db);
            });
        });
        return await dbPromise.then((values) => {
            return values;
        });
    };
    return {db}
})();