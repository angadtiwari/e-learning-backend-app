require('./utils');
const {http, app, express, io} = require('./sockets');

/**
* Listening the http server accessible for all peer within network
*/
http.listen(process.env.PORT, async () => {
    const {configRouter_v1, userRouter_v1, notificationRouter_v1, playbackRouter_v1} = require('./routers');
    
    const cors = require('cors');
    const compression = require('compression');
    const path = require('path');
    const fs = require('fs');

    app.use(compression());
    app.use(cors());
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));

    // Deploying the APIs
    app.use('/api/v1/config', configRouter_v1);
    app.use('/api/v1/user', userRouter_v1);
    app.use('/api/v1/notification', notificationRouter_v1);
    app.use('/api/v1/playback', playbackRouter_v1);

    app.use(express.static('assets'));

    console.log('Node '+process.env.NODE_ENV+' server started on '+process.env.HOST+':' +process.env.PORT);
});