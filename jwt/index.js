var fs = require('fs');
var jwt = require('jsonwebtoken');

/**
 * This method will generate the Access-Token using payload, algorithm & private.key
 * @param payload data that was been process for access-token generation
 */
var generateJWTToken = (payload) => {
    var cert = fs.readFileSync('./keys/private.key');
    return jwt.sign({
        exp: Math.floor(Date.now() / 1000) + (30*24*60*60),
        data: payload
    }, cert, { algorithm: 'HS256' });//expiresIn: 30*24*60*60(i.e. 30 days), change this acc to your need
}

/**
 * This method will validate the Access-Token, handle the TokenExpiredError & JsonWebTokenError
 * @param token Access Token to validate
 */
var validateJWTToken = (token, user) => {
    var cert = fs.readFileSync('./keys/private.key');
    try {
        var decoded = jwt.verify(token, cert, { algorithm: ['HS256'] });
        if(user !== undefined && user.username === decoded.data.username && user.password === decoded.data.password) {
            return 'success';
        } else {
            return 'error';
        }
    } catch(err) {
        if(err.name==='TokenExpiredError'){
            console.log(err);
            return 'expire';
        } else if(err.name==='JsonWebTokenError') {
            console.log(err);
            return 'error';
        }
    }
}

var findUser = (token) => {
    
}

module.exports = {generateJWTToken, validateJWTToken};