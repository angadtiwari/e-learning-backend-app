const {admin} = require('./init');

const {findOne, findAll, findWithPaging, insertOne, findOneAndUpdate, deleteOne} = require('./../db');

module.exports = (() => {
    var playbackReadyPush = async (playback) => {
        const users = await findAll('user', {'categories':{$elemMatch: {_id: playback.category._id}}}, {}, {'_id': 1, 'push_token': 1});

        const registrationTokens = users.map(ele => ele.push_token);
        const userIds = users.map(ele => ele._id);

        const pushBody = {
            id: playback.id,
            title: ''+((playback.uploader.organization_name === '')?playback.uploader.name:playback.uploader.organization_name)+' Uploaded',
            body: ''+playback.title+': Category '+playback.category.name,
            media: 'https://image.mux.com/'+playback.playback_ids[0].id+'/thumbnail.png?fit_mode=pad',
            time: Date.now().toString(),
            type: 'asset'
        };

        const message = {
            data: pushBody,
            tokens: registrationTokens,
        };
        const notificationEntry = pushBody;
        notificationEntry.playback = playback;
        notificationEntry.userIds = userIds;
        findOneAndUpdate('notification', {id: notificationEntry.id}, notificationEntry);
          
        return admin.messaging().sendMulticast(message)
            .then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
                return response;
            })
            .catch((err) => {
                return null;
            });
    };

    var liveStreamActivePush = async (playback, status) => {
        const users = await findAll('user', {'categories':{$elemMatch: {_id: playback.category._id}}}, {}, {'_id': 1, 'push_token': 1});

        const registrationTokens = users.map(ele => ele.push_token);
        const userIds = users.map(ele => ele._id);

        const pushBody = {
            id: playback.id,
            title: 'Live Stream '+status+': '+((playback.uploader.organization_name === '')?playback.uploader.name:playback.uploader.organization_name)+'',
            body: ''+playback.title+': Category '+playback.category.name,
            media: 'https://image.mux.com/'+playback.playback_ids[0].id+'/thumbnail.png?fit_mode=pad',
            time: Date.now().toString(),
            type: 'livestream'
        }

        const message = {
            data: pushBody,
            tokens: registrationTokens,
        }
        const notificationEntry = pushBody;
        notificationEntry.playback = playback;
        notificationEntry.userIds = userIds;
        findOneAndUpdate('notification', {id: notificationEntry.id}, notificationEntry);
          
        return admin.messaging().sendMulticast(message)
            .then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
                return response;
            })
            .catch((err) => {
                return null;
            });
    };

    var liveStreamCompletedPush = async (playback, status) => {
        const users = await findAll('user', {'categories':{$elemMatch: {_id: playback.category._id}}}, {}, {'_id': 1, 'push_token': 1});

        const registrationTokens = users.map(ele => ele.push_token);
        const userIds = users.map(ele => ele._id);

        const pushBody = {
            id: playback.id,
            title: 'Live Stream '+status+': '+((playback.uploader.organization_name === '')?playback.uploader.name:playback.uploader.organization_name)+'',
            body: ''+playback.title+': Category '+playback.category.name,
            media: 'https://image.mux.com/'+playback.playback_ids[0].id+'/thumbnail.png?fit_mode=pad',
            time: Date.now().toString(),
            type: 'livestream'
        }

        const message = {
            data: pushBody,
            tokens: registrationTokens,
        }
          
        return admin.messaging().sendMulticast(message)
            .then((response) => {
                console.log(response.successCount + ' messages were sent successfully');
                return response;
            })
            .catch((err) => {
                return null;
            });
    };

    return {playbackReadyPush, liveStreamActivePush, liveStreamCompletedPush};
})();