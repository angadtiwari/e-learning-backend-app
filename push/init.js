
var admin = require("firebase-admin");

var serviceAccount = require("../keys/digi_stream_service_account_key.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://omnify-e2d9c.firebaseio.com"
});

module.exports = {admin};