var express = require('express')
var configRouter = express.Router()
var {findAll} = require('./../../../db');

// middleware that is specific to this router
configRouter.use(function timeLog (req, res, next) {
  console.log('Time: ', Date.now())
  next()
})

configRouter.get('/categories', async (req, res) => {
    var categories = await findAll("category", {}, {name: 1});
    return res.status(200).json({"code":200,"status":"success","message":"list of categories","data":categories});
})

configRouter.get('/profileTypes', async (req, res) => {
    var profileTypes = await findAll("profile_type", {});
    return res.status(200).json({"code":200,"status":"success","message":"list of profile types","data":profileTypes});
})

module.exports = configRouter