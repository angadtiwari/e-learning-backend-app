var express = require('express')
var notificationRouter = express.Router()

var {findOne, findWithPaging} = require('./../../../db');
var {generateJWTToken, validateJWTToken} = require('./../../../jwt');

// middleware that is specific to this router
notificationRouter.use(async (req, res, next) => {
    var user = await findOne("user", {'access_token': req.headers.authorization});
    var tokenValidation = validateJWTToken(req.headers.authorization, user);
    switch(tokenValidation) {
        case 'success': {
            res.locals.user = user;
            next();
            break;
        }
        case 'error':
        case 'expire': {
            return res.status(401).json({"code":401,"status":"expire","message":"Session expired","data":null});
        }
        default: {
            return res.status(300).json({"code":300,"status":"unknow","message":"Unknown error occurred","data":null});
        }
    }
})

notificationRouter.get('', async (req, res) => {
    const page = parseInt(req.query.page);
    const size = parseInt(req.query.size);
    const skip = (page - 1) * size;

    if(skip < 0) {
        return res.status(501).json({"code":501,"status":"failure","message":"invalid request params","next_page":null,"data":null});
    }

    var notifications = await findWithPaging("notification", {}, skip, size, {time: -1});
    var nextPage = (notifications.length > 0 && notifications.length === size) ? (page+1) : null;
    return res.status(200).json({"code":200,"status":"success","message":"list of notifications","next_page":nextPage,"data":notifications});
})

module.exports = notificationRouter