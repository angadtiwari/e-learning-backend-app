var express = require('express');
var playbackRouter = express.Router();

const {io} = require('./../../../sockets');

var {findOne, findOneAndUpdate, insertOne, findWithPaging, deleteOne} = require('../../../db');

const {Video, Webhooks} = require('./../../../streaming');
var multer  = require('multer')
var upload = multer({ dest: 'assets/uploads/' })

var {generateJWTToken, validateJWTToken} = require('./../../../jwt');
const { ObjectId } = require('mongodb');
const bodyParser = require('body-parser');
const {playbackReadyPush, liveStreamActivePush, liveStreamCompletedPush} = require('./../../../push');

// middleware that is specific to this router
playbackRouter.use(async (req, res, next) => {
    if(req.path === '/webhook' || (req.path === '/' && req.query.isGuest === '1')) {
        next();
    } else {
        var user = await findOne("user", {'access_token': req.headers.authorization});
        var tokenValidation = validateJWTToken(req.headers.authorization, user);
        switch(tokenValidation) {
            case 'success': {
                res.locals.user = user;
                next();
                break;
            }
            case 'error':
            case 'expire': {
                return res.status(401).json({"code":401,"status":"expire","message":"Session expired","data":null});
            }
            default: {
                return res.status(300).json({"code":300,"status":"unknow","message":"Unknown error occurred","data":null});
            }
        }
    }
})

playbackRouter.post('/livestream', async (req, res) => {
    const livestream = await Video.LiveStreams.create({
        playback_policy: 'public',
        new_asset_settings: { playback_policy: 'public' }
    });
    livestream.type = 'livestream';
    livestream.uploader = res.locals.user;
    livestream.title = req.body.title;
    livestream.category = await findOne('category', {_id: ObjectId(req.body.category)});
    await insertOne('playback', livestream);
    return res.status(200).json({"code":200,"status":"success","message":"livestream started succesfully","data":livestream});
})

playbackRouter.get('', async (req, res) => {
    const page = parseInt(req.query.page);
    const size = parseInt(req.query.size);
    const skip = (page - 1) * size;

    if(skip < 0) {
        return res.status(501).json({"code":501,"status":"failure","message":"invalid request params","next_page":null,"data":null});
    }

    if(parseInt(req.query.isGuest) === 1) {
        var playbacks = await findWithPaging('playback', {'status':{$in: ['ready', 'active']}}, skip, size);
        return res.status(200).json({"code":200,"status":"success","message":"list of playbacks","next_page":null,"data":playbacks});
    } else {
        var userCategoriesIds = res.locals.user.categories.map(ele => {
            return ele._id
        });

        var playbacks = await findWithPaging('playback', {'category._id':{$in: userCategoriesIds}, 'status':{$in: ['ready', 'active']}}, skip, size);
        var nextPage = (playbacks.length > 0 && playbacks.length === size) ? (page+1) : null;
        return res.status(200).json({"code":200,"status":"success","message":"list of playbacks","next_page":nextPage,"data":playbacks});
    }
});

playbackRouter.post('/upload', upload.single('playback'), async (req, res) => {
    const asset = await Video.Assets.create({
        input: 'http://'+req.headers.host+'/uploads/'+req.file.filename,
    });
    const playbackId = await Video.Assets.createPlaybackId(asset.id, {
        policy: 'public',
    });
    asset.playback_ids = [playbackId];
    asset.type = 'asset';
    asset.uploader = res.locals.user;
    asset.title = req.body.title;
    asset.category = await findOne('category', {_id: ObjectId(req.body.category)});
    delete asset.mp4_support;
    delete asset.master_access;
    await insertOne('playback', asset);
    return res.status(200).json({"code":200,"status":"success","message":"playback uploaded succesfully","data":asset});
});

playbackRouter.post('/webhook', bodyParser.raw({type: 'application/json'}), async (req, res) => {
    try {
        const sig = req.headers['mux-signature'];
        const verifiedWebhook = Webhooks.verifyHeader(JSON.stringify(req.body), sig, process.env.WEBHOOK_SECRET);
        if(verifiedWebhook) {
            if(req.body.data.is_live) {
                return res.status(200).json({received: true});
            }
            switch(req.body.type) {
                case 'video.asset.ready': {
                    var assetIdToUpdate = req.body.data.id;
                    var playback = await findOneAndUpdate('playback', {'id':assetIdToUpdate}, {'status':'ready', 'duration': req.body.data.duration}, false);
                    await playbackReadyPush(playback);
                    io.emit('playbackReadyState', {'code':200, 'status':'ready', 'message':'playback status updated to ready state', 'data': playback});
                    break;
                }
                case 'video.asset.deleted': {
                    var assetIdToDelete = req.body.data.id;
                    await deleteOne('playback', {'id':assetIdToDelete});
                    io.emit('playbackDeletedState', {'code':201, 'status':'deleted', 'message':'playback deleted', 'data': {'id':assetIdToDelete}});
                    break;
                }
                case 'video.live_stream.connected': {
                    //TODO: live_stream is connected
                }
                case 'video.live_stream.disconnected': {
                    //TODO: live_stream is disconnected
                }
                case 'video.live_stream.recording': {
                    //TODO: live_stream is recording
                }
                case 'video.live_stream.active': {
                    //TODO: live_stream is active
                    var assetIdToUpdate = req.body.data.id;
                    var playback = await findOneAndUpdate('playback', {'id':assetIdToUpdate}, {'status':'active', 'duration': req.body.data.duration}, false);
                    await liveStreamActivePush(playback, 'Started');
                    io.emit('playbackReadyState', {'code':200, 'status':'ready', 'message':'live stream status updated to active state', 'data': playback});
                    break;
                }
                case 'video.live_stream.idle': {
                    //TODO: live_stream is idle
                }
                case 'video.asset.live_stream_completed': {
                    //TODO: live_stream is completed
                    var assetIdToUpdate = req.body.data.id;
                    var playback = await findOneAndUpdate('playback', {'id':assetIdToUpdate}, {'status':'completed', 'duration': req.body.data.duration}, false);
                    await liveStreamCompletedPush(playback, 'Completed');
                    io.emit('playbackReadyState', {'code':200, 'status':'ready', 'message':'live stream status updated to completed state', 'data': playback});
                    break;
                }
            }
            return res.status(200).json({received: true});
        } else {
            return res.status(400).send(`Webhook Error: ${err.message}`);
        }
    } catch (err) {
        return res.status(400).send(`Webhook Error: ${err.message}`);
    }
});

module.exports = playbackRouter;