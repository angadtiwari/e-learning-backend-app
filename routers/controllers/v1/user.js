var express = require('express')
const e = require('express')
var userRouter = express.Router()
const ObjectID = require('mongodb').ObjectID
var {findOne, findOneAndUpdate, findWithPaging} = require('./../../../db');

var {generateJWTToken, validateJWTToken} = require('./../../../jwt');
const { ObjectId } = require('mongodb');

// middleware that is specific to this router
userRouter.use(async (req, res, next) => {
  if(req.path === '/login') {
    if(req.body !== null && req.body !== undefined 
      && req.body !== null && req.body !== undefined 
      && req.body.email !== null && req.body.email !== undefined 
      && req.body.provider !== null && req.body.provider !== undefined
      && req.body.social_account_token !== null && req.body.social_account_token !== undefined) {
          next();
    } else {
      return res.status(401).json({"code":300,"status":"unknow","message":"Unknown error occurred","data":null});
    }
  } else {
    var user = await findOne("user", {'access_token': req.headers.authorization});
    var tokenValidation = validateJWTToken(req.headers.authorization, user);
    switch(tokenValidation) {
        case 'success': {
            res.locals.user = user;
            next();
            break;
        }
        case 'error':
        case 'expire': {
            return res.status(401).json({"code":401,"status":"expire","message":"Session expired","data":null});
        }
        default: {
            return res.status(300).json({"code":300,"status":"unknow","message":"Unknown error occurred","data":null});
        }
    }
  }
})

userRouter.post('/login', async (req, res) => {
    const appToken = generateJWTToken(req.body);
    req.body.access_token = appToken
    var user = await findOneAndUpdate("user", {'email': req.body.email}, req.body);
    return res.status(200).json({"code":200,"status":"success","message":"Login successfully","data":user});   
})

userRouter.get('', async (req, res) => {
    var user = await findOne("user", {'_id': res.locals.user._id});
    return res.status(200).json({"code":200,"status":"success","message":"User session data","data":user});   
})

userRouter.put('', async (req, res) => {
  if(req.body.organization_name !== undefined) {
    res.locals.user.organization_name = req.body.organization_name
  }
  if(req.body.categories !== undefined) {
    req.body.categories.forEach(element => {
      element._id = ObjectID(element._id);
    })
    res.locals.user.categories = req.body.categories;
  }
  if(req.body.type !== undefined) {
    req.body.type['_id'] = ObjectID(req.body.type['_id'])
    res.locals.user.type = req.body.type;
  }
  var user = await findOneAndUpdate("user", {'access_token': req.headers.authorization}, res.locals.user);
  return res.status(200).json({"code":200,"status":"success","message":"Login successfully","data":user});   
})

userRouter.get('/uploads', async (req, res) => {
  const page = parseInt(req.query.page);
  const size = parseInt(req.query.size);
  const skip = (page - 1) * size;

  if(skip < 0) {
      return res.status(501).json({"code":501,"status":"failure","message":"invalid request params","next_page":null,"data":null});
  }

  var playbacks = await findWithPaging('playback', {'uploader._id':res.locals.user._id}, skip, size);
  var nextPage = (playbacks.length > 0 && playbacks.length === size) ? (page+1) : null;
  return res.status(200).json({"code":200,"status":"success","message":"list of playbacks","next_page":nextPage,"data":playbacks});
})

module.exports = userRouter