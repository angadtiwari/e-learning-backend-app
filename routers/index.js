var configRouter_v1 = require('./controllers/v1/config');
var userRouter_v1 = require('./controllers/v1/user');
var notificationRouter_v1 = require('./controllers/v1/notification');
var playbackRouter_v1 = require('./controllers/v1/playback');

module.exports = {configRouter_v1, userRouter_v1, notificationRouter_v1, playbackRouter_v1};