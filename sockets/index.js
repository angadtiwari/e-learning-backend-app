const express = require('express');
const app  = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
/**
 * Socket.io (Node App) will emit (broadcast to all client apps) are listed belows:
 */
io.on('connection', async (socket) => {
    console.log('Socket.io Client IDs: '+Object.keys(io.sockets.sockets).join(", "));

    socket.on('disconnect', () => {
        console.log('Socket.io Client IDs: '+Object.keys(io.sockets.sockets).join(", ")); 
    });
});

module.exports = {app, http, express, io};