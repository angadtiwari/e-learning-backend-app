const Mux = require('@mux/mux-node');
const { Video }  = new Mux();
const { Webhooks } = Mux;

module.exports = {Video, Webhooks};