var fs = require('fs');
var crypto = require('crypto');

/**
 * check if the private.key exist, if not then create one with random hexadecimal
 * each time node server deployed on machine this file generated
 * this random hexadecimal key is required to generate user auth-token using jwt (json web token)
 */
if(!fs.existsSync('./keys/private.key')) {
    fs.appendFile('./keys/private.key', crypto.randomBytes(256).toString('hex'), function (err) {
        if (err) throw err;
    });
}