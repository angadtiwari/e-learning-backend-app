var fs = require('fs');

/**
 * The process object is a global that provides information about, and control over, the current Node.js process. 
 * Handlers will execute for the particular event.
 */
process.on('warning', (warning) => {
    console.warn(warning.name);    // Print the warning name
    console.warn(warning.message); // Print the warning message
    console.warn(warning.stack);   // Print the stack trace
});
process.on('uncaughtException', (err) => {
    fs.writeSync(1, `Process uncaughtException: ${err.stack}\n`);
});
process.on('exit', (code) => {
    console.log(`Process exit: Node Server exist with code: ${code}`);
});
process.on('SIGINT', function() {
    process.exit(1);
});